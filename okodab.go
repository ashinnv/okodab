package main

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

// 	Commands to change a username's bcrypt hash or
//	add a user come through as JSON and are decoded
//	into this struct.
type command struct {
	//Administration username and passhash to authenticate the change.
	Auth string
	User string

	//Username and bcrypt hash to change
	UpdateUName string
	UpdateUHash string
}

type config struct {
	ControlPort string
}

var cfg config = config{
	ControlPort: ":8099",
}

type getStr struct {
	UName string
}

func main() {

	memMap := make(map[string]string)

	go runListener(&memMap) //Listen for requests to the db for authentication
	go scanStatic(&memMap)  //Keep the memMap updated with the flatfile
	go runRam(&memMap)      //keep the memMap in memory and availablego
	go runUpdater(&memMap)  //Listen and authenticate updates to user's password hashes

	for {
		time.Sleep(time.Hour * 5000)
	}
}

func runListener(inMap *map[string]string) {

	listenFunc := lFunc(inMap)
	http.HandleFunc("/listener", listenFunc)
	http.ListenAndServe(":8081", nil)
}

func lFunc(input *map[string]string) func(writer http.ResponseWriter, request *http.Request) {
	tmpMap := *input
	return func(writer http.ResponseWriter, request *http.Request) {
		deStruct := getStr{}

		if getErr := json.NewDecoder(request.Body).Decode(&deStruct); getErr != nil {
			fmt.Errorf("%s", getErr.Error)
		}

		retData, tst := tmpMap[deStruct.UName]

		if tst {
			writer.Write([]byte(retData))
		} else {
			writer.Write([]byte("Can't find retData."))
		}
	}
}

/*
Scan through the plaintext usernames and hashed passwords to keep the
map updated.
*/
func scanStatic(masterMap *map[string]string) {

	for {
		tmpMap := *masterMap
		readMap := make(map[string]string) //For holding the file data before comparing to the masterMap

		fDat, err := ioutil.ReadFile("flatfile.csv")
		if err != nil {
			fmt.Println(fmt.Errorf("error in scanStatic: %s", err.Error()))
		}

		lines := strings.Split(string(fDat), "\n")

		//header := lines[0]
		//fmt.Println(header)

		//[username],[passhash]
		for i := 1; i < len(lines); i++ {

			splitted := strings.Split(lines[i], ",")
			if len(splitted) != 2 {
				fmt.Println("Splitted wrong len: ", splitted)
			}

			readMap[splitted[0]] = splitted[1]
		}

		//In this function, the file's data is kept vs other data that somehow got in the map
		for k, v := range readMap {
			tmpMap[k] = v
		}

		*masterMap = tmpMap
	}

}

func runRam(masterMap *map[string]string) {
	for {
		tmp := *masterMap
		//fmt.Print("\r", len(tmp), " objects in the map.")
		/*for k, v := range tmp {
			fmt.Print(k, ":", v, "---------")
		}*/
		fmt.Println(tmp)
		time.Sleep(time.Second * 1)
	}
}

func runUpdater(masterMap *map[string]string) {
	/*
		var cmd command = command{
			Auth:        "admin",
			User:        "admin",
			UpdateUName: "bill",
			UpdateUHash: "ag3ino2nid99jskhdi",
		}
	*/
	var cmd command
	ln, err := net.Listen("tcp", cfg.ControlPort)

	if err != nil {
		fmt.Println("ERROR:", err)
	}

	for {
		conn, err := ln.Accept()
		fmt.Println("HACK: AUTHORIZATION IS NOT SET IN \"runUpdater\" IN OKODAB!")
		if err != nil {
			fmt.Println("Error with accept: ", err)
		}

		tmpMap := *masterMap

		dec := gob.NewDecoder(conn)
		if err = dec.Decode(&cmd); err != nil {
			fmt.Println("DECODE ERROR IN RUNUPDATER FUNCTION: ", err)
		}

		//HACK: DO NOT LET THIS GO INTO PRODUCTION!
		if cmd.Auth == "admin" && cmd.User == "admin" {
			tmpMap[cmd.UpdateUName] = cmd.UpdateUHash
		}
	}
}

//Get profile sync daemon
